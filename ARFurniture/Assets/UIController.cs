﻿using GoogleARCore.Examples.HelloAR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour {

    public GameObject[] furniture;
    public bool placing = false;
    public bool rm1 = false;
    GameObject canvas;

    public void Exit()
    {
        Application.Quit();
    }

    public void ObjectSelect()
    {
        canvas.transform.GetChild(1).gameObject.SetActive(false);
        canvas.transform.GetChild(5).gameObject.SetActive(false);
        canvas.transform.GetChild(2).gameObject.SetActive(true);
        canvas.transform.GetChild(3).gameObject.SetActive(true);
        canvas.transform.GetChild(4).gameObject.SetActive(true);
    }

    public void RoomSelect()
    {
        canvas.transform.GetChild(1).gameObject.SetActive(false);
        canvas.transform.GetChild(5).gameObject.SetActive(false);
        canvas.transform.GetChild(6).gameObject.SetActive(true);
        canvas.transform.GetChild(7).gameObject.SetActive(true);
        canvas.transform.GetChild(8).gameObject.SetActive(true);
    }

    public void SelectPrefab(int c)
    {
        switch (c)
        {
            case 1:
                this.gameObject.GetComponent<HelloARController>().FurniturePrefab = furniture[0];
                canvas.transform.GetChild(2).gameObject.SetActive(false);
                canvas.transform.GetChild(3).gameObject.SetActive(false);
                canvas.transform.GetChild(4).gameObject.SetActive(false);
                placing = true;
                break;
            case 2:
                this.gameObject.GetComponent<HelloARController>().FurniturePrefab = furniture[1];
                canvas.transform.GetChild(2).gameObject.SetActive(false);
                canvas.transform.GetChild(3).gameObject.SetActive(false);
                canvas.transform.GetChild(4).gameObject.SetActive(false);
                placing = true;
                break;
            case 3:
                this.gameObject.GetComponent<HelloARController>().FurniturePrefab = furniture[2];
                canvas.transform.GetChild(2).gameObject.SetActive(false);
                canvas.transform.GetChild(3).gameObject.SetActive(false);
                canvas.transform.GetChild(4).gameObject.SetActive(false);
                placing = true;
                break;
            case 4:
                this.gameObject.GetComponent<HelloARController>().FurniturePrefab = furniture[3];
                canvas.transform.GetChild(6).gameObject.SetActive(false);
                canvas.transform.GetChild(7).gameObject.SetActive(false);
                canvas.transform.GetChild(8).gameObject.SetActive(false);
                rm1 = true;
                placing = true;
                break;
            case 5:
                this.gameObject.GetComponent<HelloARController>().FurniturePrefab = furniture[4];
                canvas.transform.GetChild(6).gameObject.SetActive(false);
                canvas.transform.GetChild(7).gameObject.SetActive(false);
                canvas.transform.GetChild(8).gameObject.SetActive(false);
                placing = true;
                break;
            case 6:
                this.gameObject.GetComponent<HelloARController>().FurniturePrefab = furniture[5];
                canvas.transform.GetChild(6).gameObject.SetActive(false);
                canvas.transform.GetChild(7).gameObject.SetActive(false);
                canvas.transform.GetChild(8).gameObject.SetActive(false);
                placing = true;
                break;
            default:
                break;
                    
        
        }
    }

	// Use this for initialization
	void Start () {
        canvas = GameObject.Find("Canvas");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
